# Copyright 1998 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for NSPRLib
#

COMPONENT = NSPRLib
OBJS      = ROSallocs NoThreads\
            strcat strccmp strchr strcmp strcpy strcstr strdup strlen strpbrk strstr\
            plhash plarena prlog2 prdtoa prlong prprf prmem
HDRS      = other_nspr_hdrs
CINCLUDES = ${TCPIPINC} -IRISCOSconf -INSPR.lib.ds -INSPR.lib.libc.include\
            -INSPR.pr.include -INSPR.pr.include.private -INSPR.pr.src.io
CDEFINES  = -DRISCOS -DNSPR20 -DUSE_NSPRLIB_ALLOC_REGISTRATION
VPATH     = RISCOSconf NSPR.lib.ds NSPR.lib.libc.src NSPR.pr.src.malloc\
            NSPR.pr.src.misc NSPR.pr.src.io
CFLAGS    = ${C_NOWARN_NON_ANSI_INCLUDES}

include CLibrary

exphdr.other_nspr_hdrs:
	${MKDIR} ${EXPDIR}${SEP}md
	${MKDIR} ${EXPDIR}${SEP}obsolete
	${CP} h                 ${EXPDIR}${SEP}h               ${CPFLAGS}
	${CP} RISCOSconf${SEP}h ${EXPDIR}${SEP}h               ${CPFLAGS}
	${CP} md${SEP}h         ${EXPDIR}${SEP}md${SEP}h       ${CPFLAGS}
	${CP} obsolete${SEP}h   ${EXPDIR}${SEP}obsolete${SEP}h ${CPFLAGS}

# Dynamic dependencies:
